﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ClampName : MonoBehaviour
{

	//public CanvasGroup blueThought;
	//public Text blueText;
	public Image blueImage;


    // Update is called once per frame
    void Update()
    {
		//	Vector3 namePos = Camera.main.WorldToScreenPoint(this.transform.position);
		//	blueThought.transform.position = namePos;

		Vector3 thoughtPos = Camera.main.WorldToScreenPoint(this.transform.position);
		//blueText.transform.position = thoughtPos;
		blueImage.transform.position = thoughtPos;
	}
}
