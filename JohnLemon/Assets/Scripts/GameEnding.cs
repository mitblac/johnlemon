﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class GameEnding : MonoBehaviour
{
	public float fadeDuration = 1f;
	public float displayImageDuration = 1f;
	public GameObject player;
	public CanvasGroup exitBackgroundImageCanvasGroup;
	public AudioSource exitAudio;
	public CanvasGroup caughtBackgroundImageCanvasGroup;
	public AudioSource caughtAudio;

	bool m_IsPlayerWinner;
	bool m_IsPlayerCaught;
	float m_Timer;
	bool m_HasAudioPlayed;

	//scorekeeping
	float winTally = 0;
	public float winGoal;

    //dialogue
    public CanvasGroup RedText;
	public CanvasGroup GreenText;
	public CanvasGroup OrangeText;
	public CanvasGroup YellowText;
	public CanvasGroup PurpleText;
	public CanvasGroup PinkText;
	public CanvasGroup BlueText;

	//fruit icons
	public GameObject appleIcon;
	public GameObject pearIcon;
	public GameObject peachIcon;
	public GameObject bananaIcon;
	public GameObject plumIcon;
	public GameObject melonIcon;
	public GameObject berryIcon;



	public void CaughtPlayer()
	{
		m_IsPlayerCaught = true;
	}

    //Giving the fruits to the Ghosts and making the dialogue appear:

    public void GiveApple()
	{
		winTally += 1; //adds +1 to the Win Tally
		RedText.alpha = 1; //makes the dialogue visiible
		appleIcon.SetActive(false);
	}

	public void GivePear()
	{
		winTally += 1;
		GreenText.alpha = 1;
		pearIcon.SetActive(false);
	}

	public void GivePeach()
	{
		winTally += 1;
		OrangeText.alpha = 1;
		peachIcon.SetActive(false);
	}

	public void GiveBanana()
	{
		winTally += 1;
		YellowText.alpha = 1;
		bananaIcon.SetActive(false);
	}

	public void GivePlum()
	{
		winTally += 1;
		PurpleText.alpha = 1;
		plumIcon.SetActive(false);
	}

	public void GiveMelon()
	{
		winTally += 1;
		PinkText.alpha = 1;
		melonIcon.SetActive(false);
	}

	public void GiveBerry()
	{
		winTally += 1;
		BlueText.alpha = 1;
		berryIcon.SetActive(false);
	}


	///

	void Update()
	{

		if (m_IsPlayerWinner)
		{
			EndLevel(exitBackgroundImageCanvasGroup, false, exitAudio);
		}
		else if (m_IsPlayerCaught)
		{
			EndLevel(caughtBackgroundImageCanvasGroup, true, caughtAudio);
		}

        //dialogue - makes the dialogie boxes go away when space is pressed

		bool down = Input.GetKeyDown(KeyCode.Space);

		if (down)
		{
			RedText.alpha = 0;
			GreenText.alpha = 0;
			OrangeText.alpha = 0;
			YellowText.alpha = 0;
			PurpleText.alpha = 0;
			PinkText.alpha = 0;
			BlueText.alpha = 0;

			if (winTally == winGoal) //initiates the Win Screen here (so that win screen does not occur before player has read the final dialogue box)
			{
				m_IsPlayerWinner = true;
			}
		}
	}

	void EndLevel(CanvasGroup imageCanvasGroup, bool doRestart, AudioSource audioSource)
	{
		if (!m_HasAudioPlayed)
		{
			audioSource.Play();
			m_HasAudioPlayed = true;
		}

		m_Timer += Time.deltaTime;

		imageCanvasGroup.alpha = m_Timer / fadeDuration;

		if (m_Timer > fadeDuration + displayImageDuration)
		{
			if (doRestart)
			{
				SceneManager.LoadScene(2);
			}
			else
			{
				Application.Quit();
			}
		}
	}
}
