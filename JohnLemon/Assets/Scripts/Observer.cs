﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Observer : MonoBehaviour
{
	public Transform player;
	public GameEnding gameEnding;
	bool m_IsPlayerInRange;

	public GameObject _PlayerMovement;
	private PlayerMovement script;
	public GameObject _selfGhost;
	public GameObject _thought;

	void Start()
	{

		script = _PlayerMovement.GetComponent<PlayerMovement>(); //sets variable script to the PlayerMovement script attached to object _PlayerMovement
	}

	void OnTriggerEnter(Collider other)
	{
		if (other.transform == player)
		{
			m_IsPlayerInRange = true;
		}
	}

	void OnTriggerExit(Collider other)
	{
		if (other.transform == player)
		{
			m_IsPlayerInRange = false;
		}
	}



	void Update()
	{
		if (m_IsPlayerInRange)
		{
			Vector3 direction = player.position - transform.position + Vector3.up;

			Ray ray = new Ray(transform.position, direction);
			RaycastHit raycastHit;

			if (Physics.Raycast(ray, out raycastHit))
			{
				if (raycastHit.collider.transform == player)
				{
                    if (_selfGhost.gameObject.CompareTag("Red Ghost")) //checks to see if the Ghost is the Red Ghost
					{
						if (script.hasApple == true) //checks to see if player has collected the Apple (the fruit the Red Ghost wants)
						{
							Debug.Log("Yummy!"); //for programmer to check to see if it works
							gameEnding.GiveApple(); //tells the GameEnding script to run the function GiveApple
							_selfGhost.SetActive(false); //makes the ghost disappear
							_thought.SetActive(false);
						}
						else
						{
							gameEnding.CaughtPlayer();
						}
					}
					else if (_selfGhost.gameObject.CompareTag("Green Ghost")) //PEAR
					{
						if (script.hasPear == true)
						{
							Debug.Log("Yum-O!");
							gameEnding.GivePear();
							_selfGhost.SetActive(false);
							_thought.SetActive(false);
						}
						else
						{
							gameEnding.CaughtPlayer();
						}
					}
					else if (_selfGhost.gameObject.CompareTag("Orange Ghost")) //PEACH
					{
						if (script.hasPeach == true)
						{
							Debug.Log("Delish!");
							gameEnding.GivePeach();
							_selfGhost.SetActive(false);
							_thought.SetActive(false);
						}
						else
						{
							gameEnding.CaughtPlayer();
						}
					}
					else if (_selfGhost.gameObject.CompareTag("Yellow Ghost")) //BANANA
					{
						if (script.hasBanana == true)
						{
							Debug.Log("Yum");
							gameEnding.GiveBanana();
							_selfGhost.SetActive(false);
							_thought.SetActive(false);
						}
						else
						{
							gameEnding.CaughtPlayer();
						}
					}
					else if (_selfGhost.gameObject.CompareTag("Purple Ghost")) //PLUM
					{
						if (script.hasPlum == true)
						{
							Debug.Log("Yum, a plum!");
							gameEnding.GivePlum();
							_selfGhost.SetActive(false);
							_thought.SetActive(false);
						}
						else
						{
							gameEnding.CaughtPlayer();
						}
					}
					else if (_selfGhost.gameObject.CompareTag("Pink Ghost")) //MELON
					{
						if (script.hasMelon == true)
						{
							Debug.Log("I'm allergic, but ok");
							gameEnding.GiveMelon();
							_selfGhost.SetActive(false);
							_thought.SetActive(false);
						}
						else
						{
							gameEnding.CaughtPlayer();
						}
					}
					else if (_selfGhost.gameObject.CompareTag("Blue Ghost")) //BERRY
					{
						if (script.hasBerry == true)
						{
							Debug.Log("Yeet!");
							gameEnding.GiveBerry();
							_selfGhost.SetActive(false);
							_thought.SetActive(false);
						}
						else
						{
							gameEnding.CaughtPlayer();
						}
					}
				}
			}
		}
	}
}
