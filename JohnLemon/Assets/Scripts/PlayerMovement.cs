﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PlayerMovement : MonoBehaviour
{

	public float turnSpeed = 20f;

	Animator m_Animator;
	Rigidbody m_Rigidbody;
	AudioSource m_AudioSource; //why wouldn't I just make this public and assign it in the inspector?
	Vector3 m_Movement;
	Quaternion m_Rotation = Quaternion.identity;

	// Fruits
	public Rigidbody apple_rb;
	public bool hasApple = false;
	public Rigidbody pear_rb;
	public bool hasPear = false;
	public Rigidbody peach_rb;
	public bool hasPeach = false;
	public Rigidbody banana_rb;
	public bool hasBanana = false;
	public Rigidbody plum_rb;
	public bool hasPlum = false;
	public Rigidbody melon_rb;
	public bool hasMelon = false;
	public Rigidbody berry_rb;
	public bool hasBerry = false;

	//Fruit Icons
	public GameObject appleIcon;
	public GameObject pearIcon;
	public GameObject peachIcon;
	public GameObject bananaIcon;
	public GameObject plumIcon;
	public GameObject melonIcon;
	public GameObject berryIcon;

	// Start is called before the first frame update
	void Start()
	{
		m_Animator = GetComponent<Animator>();
		m_Rigidbody = GetComponent<Rigidbody>();
		m_AudioSource = GetComponent<AudioSource>();

		appleIcon.SetActive(false);
		pearIcon.SetActive(false);
		peachIcon.SetActive(false);
		bananaIcon.SetActive(false);
		plumIcon.SetActive(false);
		melonIcon.SetActive(false);
		berryIcon.SetActive(false);
	}

	// Update is called once per frame
	void FixedUpdate()
	{
		float horizontal = Input.GetAxis("Horizontal");
		float vertical = Input.GetAxis("Vertical");

		//Vector
		m_Movement.Set(horizontal, 0f, vertical);
		m_Movement.Normalize(); //makes it so that the player goes maximum magnitude 1 on diagonal

		//Input
		bool hasHorizontalInput = !Mathf.Approximately(horizontal, 0f);
		bool hasVerticalInput = !Mathf.Approximately(vertical, 0f);
		bool isWalking = hasHorizontalInput || hasVerticalInput;
		m_Animator.SetBool("IsWalking", isWalking);
		if (isWalking)
		{
			if (!m_AudioSource.isPlaying)
			{
				m_AudioSource.Play();
			}
		}
		else
		{
			m_AudioSource.Stop();
		}

		//Rotation
		Vector3 desiredForward = Vector3.RotateTowards(transform.forward, m_Movement, turnSpeed * Time.deltaTime, 0f);
		m_Rotation = Quaternion.LookRotation(desiredForward);
	}

	void OnAnimatorMove()
	{
		m_Rigidbody.MovePosition(m_Rigidbody.position + m_Movement * m_Animator.deltaPosition.magnitude);
		m_Rigidbody.MoveRotation(m_Rotation);
	}

	private void OnTriggerEnter(Collider other)
	{
		if (other.gameObject.CompareTag("Fruit"))
		{

            if (other.gameObject.GetComponent<Rigidbody>() == apple_rb)
			{
				hasApple = true;
				Debug.Log("Has Apple");
				appleIcon.SetActive(true);
			}
            else if (other.gameObject.GetComponent<Rigidbody>() == pear_rb)
			{
				hasPear = true;
				Debug.Log("Has Pear");
				pearIcon.SetActive(true);
			}
			else if (other.gameObject.GetComponent<Rigidbody>() == peach_rb)
			{
				hasPeach = true;
				Debug.Log("Has Peach");
				peachIcon.SetActive(true);
			}
			else if (other.gameObject.GetComponent<Rigidbody>() == banana_rb)
			{
				hasBanana = true;
				Debug.Log("Has Banana");
				bananaIcon.SetActive(true);
			}
			else if (other.gameObject.GetComponent<Rigidbody>() == plum_rb)
			{
				hasPlum = true;
				Debug.Log("Has Plum");
				plumIcon.SetActive(true);
			}
			else if (other.gameObject.GetComponent<Rigidbody>() == melon_rb)
			{
				hasMelon = true;
				Debug.Log("Has Melon");
				melonIcon.SetActive(true);
			}
			else if (other.gameObject.GetComponent<Rigidbody>() == berry_rb)
			{
				hasBerry = true;
				Debug.Log("Has Berry");
				berryIcon.SetActive(true);
			}

			other.gameObject.SetActive(false); //make fruit disappear
		}
	}

}
